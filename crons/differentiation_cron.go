package crons

import (
	"encoding/json"
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"
	"net/http"
	"time"
)

/*
with last_timestamps as (
 	select  max(timestamp)as timestamp from "mutual_prices" mp1
 	group by "base_asset_id","counter_asset_id"
),pre_last_timestamps as (
 	select  max(timestamp)as timestamp from "mutual_prices" mp1
	where timestamp not in (select * from last_timestamps)
 	group by "base_asset_id","counter_asset_id"
)

select base_asset_id ,counter_asset_id ,"id",mp1.close_price - COALESCE ((
		select close_price from "mutual_prices" mp2
		where mp2.timestamp in (select * from pre_last_timestamps)
		and mp2.base_asset_id = mp1.base_asset_id
		and mp1.counter_asset_id  = mp2.counter_asset_id
	),0) as close_price_change , mp1.close_price as last_recorded_price from "mutual_prices" mp1
	where mp1.timestamp in (
 		select * from last_timestamps
	)

*/
func Differenctiate(w http.ResponseWriter, r *http.Request) {
	var difference []domain.Differentiate
	var estimated_duration = 3 * time.Minute
	var started_at time.Time = time.Now()

	config.Db.Raw(`
		with last_timestamps as (
			select  max(timestamp)as timestamp from "mutual_prices" mp1
			group by "base_asset_id","counter_asset_id"
		),pre_last_timestamps as (
			select  max(timestamp)as timestamp from "mutual_prices" mp1
			where timestamp not in (select * from last_timestamps)
			group by "base_asset_id","counter_asset_id"
		)
		select base_asset_id ,counter_asset_id ,mp1.close_price - COALESCE ((
				select close_price from "mutual_prices" mp2
				where mp2.timestamp in (select * from pre_last_timestamps) 
				and mp2.base_asset_id = mp1.base_asset_id 
				and mp1.counter_asset_id  = mp2.counter_asset_id
			),0) as close_price_change , mp1.close_price as last_recorded_price from "mutual_prices" mp1
			where mp1.timestamp in (
				select * from last_timestamps
			)
	`).Scan(&difference)
	if err := config.Db.Create(&difference).Error; err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	var finished_at time.Time = time.Now()

	cronSum := models.CronSummaryDto{
		EstimatedDuration: estimated_duration.Seconds(),
		Maintainer:        "masoud moghini",
		ProcessedDuration: finished_at.Sub(started_at).Seconds(),
		FinishedAt:        finished_at,
		RowsInserted:      int16(len(difference)),
		RowsUpdated:       int16(len(difference)),
		StartedAt:         started_at,
		Status:            true,
	}
	json.NewEncoder(w).Encode(cronSum)

}
