package crons

import (
	"encoding/json"
	"fmt"
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var baseAddress string

func init() {
	baseAddress = config.Config.Horizon
}
func MutualPriceCron(w http.ResponseWriter, r *http.Request) {
	var started_at time.Time = time.Now()
	var estimated_duration = 3 * time.Minute
	var assets []domain.Asset
	var rows_created int16 = 0
	config.Db.Model(&domain.Asset{}).Select("id,code,issuer").Find(&assets)
	var baseAssetAddress, counterAssetAddress string
	_ = baseAssetAddress
	_ = counterAssetAddress
	for i, x := range assets {

		if strings.ToLower(x.Code) == "pmn" {
			fmt.Println("pmn")
			baseAssetAddress = "base_asset_type=native"
		} else if len(x.Code) <= 4 {
			baseAssetAddress = fmt.Sprintf("base_asset_type=credit_alphanum4&base_asset_code=%s&base_asset_issuer=%s", x.Code, x.Issuer)
		} else {
			baseAssetAddress = fmt.Sprintf("base_asset_type=credit_alphanum12&base_asset_code=%s&base_asset_issuer=%s", x.Code, x.Issuer)
		}
		for j, y := range assets {

			if i == j {
				continue
			}

			//_resp = append(_resp, map[string]interface{}{"code": x.Code, "issuer": x.Issuer})
			if strings.ToLower(y.Code) == "pmn" {
				counterAssetAddress = "counter_asset_type=native"
			} else if len(y.Code) <= 4 {
				counterAssetAddress = fmt.Sprintf("counter_asset_type=credit_alphanum4&counter_asset_code=%s&counter_asset_issuer=%s", y.Code, y.Issuer)
			} else {
				counterAssetAddress = fmt.Sprintf("counter_asset_type=credit_alphanum12&counter_asset_code=%s&counter_asset_issuer=%s", y.Code, y.Issuer)
			}
			var trade_aggregations models.HorizonTradeAggResponse
			url := fmt.Sprintf(baseAddress+"/trade_aggregations?%s&%s&resolution=86400000&order=desc", baseAssetAddress, counterAssetAddress)
			if strings.ToLower(x.Code) == "pmn" && strings.ToLower(y.Code) == "skyc" {
				fmt.Println(url)
			}
			_resp, _ := http.Get(url)
			json.NewDecoder(_resp.Body).Decode(&trade_aggregations)
			//fmt.Println(_resp.String())
			//_resp.JSON(trade_aggregations)

			if len(trade_aggregations.Embedded.Records) > 0 {
				record := trade_aggregations.Embedded.Records[0]

				base_volume, _ := strconv.ParseFloat(record.BaseVolume, 64)
				counter_volume, _ := strconv.ParseFloat(record.CounterVolume, 64)
				average_price, _ := strconv.ParseFloat(record.AveragePrice, 64)
				hight_price, _ := strconv.ParseFloat(record.HighPrice, 64)
				low_price, _ := strconv.ParseFloat(record.LowPrice, 64)
				open_price, _ := strconv.ParseFloat(record.OpenPrice, 64)
				close_price, _ := strconv.ParseFloat(record.ClosePrice, 64)

				newMutualPrice := &domain.MutualPrice{
					Timestamp:      time.Unix(record.Timestamp/1000, 0),
					BaseAssetID:    x.ID,
					CounterAssetID: y.ID,
					BaseVolume:     base_volume,
					CounterVolume:  counter_volume,
					Avg:            average_price,
					High:           hight_price,
					Low:            low_price,
					Open:           open_price,
					Close:          close_price,
				}
				_num_created, err := newMutualPrice.Create()
				if err != nil {
					cronSummary := models.CronSummaryDto{
						Status:     false,
						Maintainer: "masoud moghini",
					}
					resp, _ := json.Marshal(cronSummary)
					fmt.Println(string(resp))
					//fmt.Fprint(w, string(resp))
				} else {
					rows_created += _num_created
				}
			} else {
				fmt.Println("not contained")
			}
		}
	}
	var finished_at time.Time = time.Now()
	var processed_duration time.Duration = finished_at.Sub(started_at)

	if rows_created > 0 {
		w.WriteHeader(http.StatusCreated)
	} else {
		w.WriteHeader(http.StatusAccepted)

	}
	w.Header().Add("Content-Type", "application/json")

	cronSummary := models.CronSummaryDto{
		StartedAt:         started_at,
		FinishedAt:        finished_at,
		Status:            true,
		EstimatedDuration: estimated_duration.Seconds(),
		ProcessedDuration: processed_duration.Seconds(),
		RowsInserted:      rows_created,
		RowsUpdated:       0,
		RowsDeleted:       0,
		TotalRowsAffected: rows_created,
		Maintainer:        "masoud moghini",
	}
	json.NewEncoder(w).Encode(cronSummary)
}
