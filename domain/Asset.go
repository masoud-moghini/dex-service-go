package domain

type Asset struct {
	ID            int    `gorm:"column:id"`
	Code          string `gorm:"column:code"`
	Image         string `gorm:"column:image"`
	Issuer        string `gorm:"column:issuer"`
	TokenLimit    string `gorm:"column:token_limit"`
	Name          string `gorm:"column:name"`
	NameFa        string `gorm:"column:name_fa"`
	Domain        string `gorm:"column:domain"`
	Type          string `gorm:"column:type"`
	Specifier     string `gorm:"column:specifier"`
	CreatedAt     string `gorm:"column:created_at"`
	UpdatedAt     string `gorm:"column:updated_at"`
	Anchor        string `gorm:"column:anchor"`
	WhitePaper    string `gorm:"column:whitepaper"`
	Contract      string `gorm:"column:contract"`
	Decimal       string `gorm:"column:decimal"`
	Backed        string `gorm:"column:backed"`
	Category      string `gorm:"column:category"`
	Refund        string `gorm:"column:refund"`
	Display       string `gorm:"column:display"`
	DescEn        string `gorm:"column:desc_en"`
	DescFa        string `gorm:"column:desc_fa"`
	DisplayRefund string `gorm:"column:display_refund"`
	DisplaySale   string `gorm:"column:display_sale"`
	DisplayInfo   string `gorm:"column:display_info"`
}
