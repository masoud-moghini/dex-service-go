package domain

import (
	"errors"
	"fmt"
	"kuknos/dex/config"
	"time"

	"gorm.io/gorm"
)

type MutualPrice struct {
	gorm.Model
	Timestamp      time.Time `gorm:"column:timestamp"`
	BaseAssetID    int
	BaseAsset      Asset
	CounterAssetID int
	CounterAsset   Asset
	BaseVolume     float64 `gorm:"column:base_volume"`
	CounterVolume  float64 `gorm:"column:counter_volume"`
	Avg            float64 `gorm:"column:average_price"`
	Low            float64 `gorm:"column:lowest_price"`
	High           float64 `gorm:"column:highest_price"`
	Open           float64 `gorm:"column:open_price"`
	Close          float64 `gorm:"column:close_price"`
}

func (inserted_price *MutualPrice) Create() (int16, error) {
	//create asset with specified base and counter if timestamp is not existed
	var rows_created int16 = 0
	var last_pair_inserted MutualPrice
	if err := config.Db.Model(&MutualPrice{}).
		Where(&MutualPrice{
			BaseAssetID:    inserted_price.BaseAssetID,
			CounterAssetID: inserted_price.CounterAssetID,
		}).
		Order("timestamp desc").
		First(&last_pair_inserted).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) { // FOR THE CASE OF FIRST INSERTION
			if cr_err := config.Db.Create(inserted_price).Error; cr_err != nil {
				fmt.Println(cr_err.Error())
			}
			rows_created++
		} else { //ERROR HAPPENS
			return 0, err
		}
	} else {
		config.Db.Create(inserted_price)
		rows_created++
	}
	return rows_created, nil
}
