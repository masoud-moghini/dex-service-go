package domain

import "kuknos/dex/config"

func init() {
	config.Db.AutoMigrate(&MutualPrice{}, &Account{}, &Asset{}, &User{}, &Offer{}, &Differentiate{})
}
