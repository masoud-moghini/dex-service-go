package domain

import (
	"gorm.io/gorm"
)

type Account struct {
	gorm.Model
	Name   string `gorm:"name"`
	Public string `gorm:"public_key"`
	UserID int    `gorm:"user_id"`
	User   User
}
