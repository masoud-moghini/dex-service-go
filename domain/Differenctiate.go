package domain

import (
	"gorm.io/gorm"
)

type Differentiate struct {
	gorm.Model
	BaseAssetID       int `gorm:"base_asset_id"`
	BaseAsset         Asset
	CounterAssetID    int `gorm:"counter_asset_id"`
	CounterAsset      Asset
	LastRecordedPrice float64 `gorm:"last_recorded_price"`
	ChoicePriceChange float64 `gorm:"choice_price_change"`
}
