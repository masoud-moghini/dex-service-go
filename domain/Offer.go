package domain

import (
	"gorm.io/gorm"
)

type Offer struct {
	gorm.Model
	AccountID      int     `json:"account_id"`
	LedgerID       uint64  `json:"ledger_id"`
	SellingAssetID int     `json:"selling_asset" gorm:"references:ID"`
	Selling        Asset   `json:"selling_asset" gorm:"foreignKey:SellingAssetID"`
	BuyingAssetID  int     `json:"buying_asset"  gorm:"references:ID"`
	Buying         Asset   `json:"buying_asset"  gorm:"foreignKey:BuyingAssetID"`
	Type           string  `json:"type"` //buy or sell
	Amount         float64 `json:"amount"`
	Price          float64 `json:"price"`
}
