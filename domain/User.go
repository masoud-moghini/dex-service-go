package domain

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	FirstName    string `gorm:"first_name"`
	LastName     string `gorm:"last_name"`
	Email        string `gorm:"email"`
	PhoneNumber  string `gorm:"phone_number"`
	NationalCode string `gorm:"national_code"`
	Iban         string `gorm:"iban"`
}
