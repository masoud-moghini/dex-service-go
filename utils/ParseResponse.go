package utils

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/stellar/go/xdr"
)

func initialHandling(result_xdr string) xdr.OperationResult {

	rawr := strings.NewReader(result_xdr)
	b64r := base64.NewDecoder(base64.StdEncoding, rawr)
	var tx xdr.TransactionResult
	_, err := xdr.Unmarshal(b64r, &tx)
	if err != nil {
		fmt.Println(err)
	}
	opr := (*tx.Result.Results)[0]
	return opr
}

func ParseBuyOffer(xdr string) *xdr.OfferEntry {
	opr := initialHandling(xdr)
	fo := opr.Tr.ManageBuyOfferResult
	res, succ := fo.GetSuccess()
	if !succ {
		fmt.Println(succ)
	}
	of, succ := res.Offer.GetOffer()
	if !succ {
		fmt.Println(succ)
	}
	return &of
}
func ParseSellOffer(xdr string) *xdr.OfferEntry {
	opr := initialHandling(xdr)
	fo := opr.Tr.ManageSellOfferResult
	res, succ := fo.GetSuccess()
	if !succ {
		fmt.Println(succ)
	}
	of, succ := res.Offer.GetOffer()
	if !succ {
		fmt.Println(succ)
	}
	return &of
}
