package services

import (
	"encoding/json"
	"fmt"
	"kuknos/dex/config"
	"kuknos/dex/models"
	"kuknos/dex/repositories"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/stellar/go/clients/horizonclient"
)

func CreateOffer(w http.ResponseWriter, r *http.Request) {
	client := horizonclient.Client{
		HorizonURL: config.Config.Horizon,
	}

	var offer models.OfferDto
	err := json.NewDecoder(r.Body).Decode(&offer)
	if err != nil {
		fmt.Printf("\n\n\nhi\n\n\n\n")
		fmt.Println(err.Error)
	}
	_, err = govalidator.ValidateStruct(offer)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "فیلد های ضروری ارسال نشده اند",
				En: "required field are not being sent",
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}

	result, err := client.SubmitTransactionXDR(offer.TX)
	if err == nil {
		w.WriteHeader(http.StatusCreated)
		d_result, _ := json.Marshal(result)
		SuccessResponse, _ := models.ModelizeSingleResponse(true, d_result, err)
		json.NewEncoder(w).Encode(SuccessResponse)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		_, FailureResponse := models.ModelizeResponse(false, nil, err)
		json.NewEncoder(w).Encode(FailureResponse)
	}
}
func ListOffers(w http.ResponseWriter, r *http.Request) {
	fmt.Println("hi")
	var accnt *models.AccountDto
	token := r.Header.Get("token")
	fmt.Println("hi2")
	account_id, _ := strconv.Atoi(token)
	accnt, err := repositories.FindAccountById(account_id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, FailureResponse := models.ModelizeResponse(false, nil, err)
		json.NewEncoder(w).Encode(FailureResponse)
		return
	}
	public_account := accnt.Public
	fmt.Println(public_account)
	client := horizonclient.Client{
		HorizonURL: config.Config.Horizon,
	}
	offerRequest := horizonclient.OfferRequest{
		ForAccount: public_account,
		Cursor:     "now",
		Order:      horizonclient.OrderDesc,
	}
	tradeRequest := horizonclient.TradeRequest{
		ForAccount: public_account,
		Cursor:     "now",
		Order:      horizonclient.OrderDesc,
	}
	offers, err := client.Offers(offerRequest)
	if err != nil {
		fmt.Println(err.Error)
	}
	trades, err := client.Trades(tradeRequest)
	if err != nil {
		fmt.Println(err.Error)
	}
	tradesRecords := trades.Embedded.Records
	offersRecords := offers.Embedded.Records

	response := map[string]interface{}{"trades": tradesRecords, "offers": offersRecords}
	json.NewEncoder(w).Encode(response)
}
