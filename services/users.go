package services

import (
	"encoding/json"
	"kuknos/dex/domain"
	"kuknos/dex/models"
	"kuknos/dex/repositories"
	"net/http"
	"strconv"

	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
)

func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user models.UserDto
	json.NewDecoder(r.Body).Decode(&user)

	_, err := govalidator.ValidateStruct(user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "فیلد های ضروری ارسال نشده اند",
				En: "required field are not being sent",
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}

	err = repositories.SaveUser(&user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, FailureResponse := models.ModelizeResponse(false, nil, err)
		json.NewEncoder(w).Encode(FailureResponse)
	}
	w.WriteHeader(http.StatusCreated)
	SuccessResponse, _ := models.ModelizeResponse(true, nil, err)
	json.NewEncoder(w).Encode(SuccessResponse)
}

func UserInfo(w http.ResponseWriter, r *http.Request) {
	variables := mux.Vars(r)
	var user *models.UserDto
	var oldUser *domain.User
	var err error
	var updatedUser models.UserDto

	user_id, _ := strconv.Atoi(variables["user_id"])
	oldUser, user, err = repositories.FindUser(user_id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, FailureResponse := models.ModelizeResponse(false, nil, err)
		if err1 := json.NewEncoder(w).Encode(FailureResponse); err1 != nil {
			panic(err1.Error())
		}
		return
	}
	if r.Method == "GET" {
		w.WriteHeader(http.StatusAccepted)
		resp, _ := json.Marshal(*user)
		SuccessResponse, _ := models.ModelizeSingleResponse(true, resp, err)
		json.NewEncoder(w).Encode(SuccessResponse)
		return
	} else if r.Method == "PUT" {
		json.NewDecoder(r.Body).Decode(&updatedUser)
		if err := repositories.UpdateUser(oldUser, &updatedUser).Error; err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, FailureResponse := models.ModelizeResponse(false, nil, err)
			if err1 := json.NewEncoder(w).Encode(FailureResponse); err1 != nil {
				panic(err1.Error())
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		resp, _ := json.Marshal(updatedUser)
		SuccessResponse, _ := models.ModelizeSingleResponse(true, resp, err)
		json.NewEncoder(w).Encode(SuccessResponse)
	}

}
