package services

import (
	"encoding/json"
	"kuknos/dex/config"
	"kuknos/dex/models"
	"kuknos/dex/repositories"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/stellar/go/clients/horizonclient"
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	var accountDto, account models.AccountDto
	var err error
	json.NewDecoder(r.Body).Decode(&accountDto)
	if err, account = repositories.CreateAccount(accountDto); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusCreated)
	byteAccount, _ := json.Marshal(account)
	SuccessResponse, _ := models.ModelizeSingleResponse(true, byteAccount, nil)
	json.NewEncoder(w).Encode(SuccessResponse)
}

func List(w http.ResponseWriter, r *http.Request) {
	var accounts []models.AccountDto
	variables := mux.Vars(r)
	user_id, _ := strconv.Atoi(variables["user_id"])
	if err := repositories.FindUserAccounts(user_id, &accounts); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	byte_accounts, _ := json.Marshal(accounts)
	SuccessResponse, _ := models.ModelizeResponse(true, byte_accounts, nil)
	json.NewEncoder(w).Encode(SuccessResponse)
}
func UserDetail(w http.ResponseWriter, r *http.Request) {
	public_id := r.Header.Get("public_id")
	_accountData := horizonclient.AccountRequest{
		AccountID: public_id,
	}
	client := horizonclient.Client{
		HorizonURL: config.Config.Horizon,
	}
	accountData, err := client.AccountDetail(_accountData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	byteAccountData, _ := json.Marshal(accountData)
	SuccessResponse, _ := models.ModelizeSingleResponse(true, byteAccountData, nil)
	json.NewEncoder(w).Encode(SuccessResponse)
}
