package services

import (
	"encoding/json"
	"fmt"
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"
	"log"
	"net/http"
	"strings"

	"gorm.io/gorm"
)

var Db *gorm.DB

func init() {
	Db = config.Db
}

func Differenctiates(w http.ResponseWriter, r *http.Request) {
	// config.Db.Raw(`
	// 	select base_asset_id,counter_asset_id,choice_price_change,last_price_recorded
	// `)
	//var differentiates []models.Differentiate
	var differentiates []map[string]interface{}
	var query string
	counterCode := ""
	queryParams := r.URL.Query()
	if len(queryParams["counter"]) > 0 {
		counterCode = queryParams["counter"][0]
	}
	log.Println("hei")
	log.Println(counterCode == "")
	if counterCode == "" {
		query = `
			SELECT as1.code as base_code, as2.code as counter_code,choice_price_change,last_recorded_price,max(differentiates.created_at) 
			FROM "differentiates" 
			join assets as1 on as1.id =differentiates.base_asset_id 
			join assets as2 on as2.id =differentiates.counter_asset_id  
			GROUP BY (as1.code,as2.code,choice_price_change,last_recorded_price)
		`
	} else {
		query = fmt.Sprintf(`
			SELECT as1.code as base_code, as2.code as counter_code,choice_price_change,last_recorded_price,max(differentiates.created_at) 
			FROM "differentiates" 
			join assets as1 on as1.id =differentiates.base_asset_id 
			join assets as2 on as2.id =differentiates.counter_asset_id  
			WHERE as2.code='%s'
			GROUP BY (as2.created_at,as1.code,as2.code,choice_price_change,last_recorded_price)
			ORDER BY as2.created_at
			LIMIT(5)
		`, strings.ToUpper(counterCode))
	}
	fmt.Println(query)

	if err := config.Db.Model(&domain.Differentiate{}).
		Raw(query).
		Scan(&differentiates).Error; err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}

	fmt.Println("hello")
	if _, err := fmt.Println(differentiates[0]); err != nil {
		fmt.Println(err)
	}
	w.WriteHeader(http.StatusAccepted)
	byte_differentiates, _ := json.Marshal(differentiates)
	SuccessResponse, _ := models.ModelizeResponse(true, byte_differentiates, nil)
	json.NewEncoder(w).Encode(SuccessResponse)

}

func LastPrices(w http.ResponseWriter, r *http.Request) {

	pathParamters := r.URL.Query()
	counter_asset_code := pathParamters["counter_asset_code"][0]
	var records []models.MutualPriceDto
	query := Db.Model(&domain.MutualPrice{}).Select(
		"mutual_prices.base_asset_id," +
			"ass1.code as base_asset," +
			"mutual_prices.counter_asset_id," +
			"mutual_prices.base_volume," +
			"ass2.code as counter_asset," +
			"mutual_prices.close_price as close",
	).Joins("join assets ass1 on mutual_prices.base_asset_id = ass1.id").Joins("join assets ass2 on mutual_prices.counter_asset_id = ass2.id")
	if counter_asset_code != "" {
		query = query.Where("lower(ass2.code) = ?", counter_asset_code)
	}
	query.Scan(&records)
	if records == nil {
		records = []models.MutualPriceDto{}
	}
	json.NewEncoder(w).Encode(records)
}

func Assets(w http.ResponseWriter, r *http.Request) {
	var assets []models.Asset
	var _assets []domain.Asset
	if err := config.Db.Model(&domain.Asset{}).Find(&_assets).Error; err != nil {
		w.WriteHeader(http.StatusBadRequest)
		response := models.WalletServiceFailureResponse{
			Meta: models.Meta{
				Success: false,
			},
			Message: models.Message{
				Fa: "خطایی رخ داده",
				En: err.Error(),
			},
		}
		json.NewEncoder(w).Encode(response)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	for i := 0; i < len(_assets); i++ {
		assets = append(assets, models.Asset{
			Issuer: _assets[i].Issuer,
			Code:   _assets[i].Code,
		})
	}
	byte_assets, _ := json.Marshal(assets)
	SuccessResponse, _ := models.ModelizeResponse(true, byte_assets, nil)
	json.NewEncoder(w).Encode(SuccessResponse)
}
