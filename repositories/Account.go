package repositories

import (
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"
)

func CreateAccount(_account models.AccountDto) (error, models.AccountDto) {
	account := domain.Account{
		Name:   _account.Name,
		Public: _account.Public,
		UserID: _account.UserId,
	}
	err := config.Db.Model(&domain.Account{}).Create(&account).Error
	createdAccountId := int(account.ID)
	accountDto := models.AccountDto{
		ID:     createdAccountId,
		Public: account.Public,
	}
	return err, accountDto
}
func FindUserAccounts(user_id int, accounts *[]models.AccountDto) error {
	err := config.Db.Model(&domain.Account{}).Find(accounts, "user_id = ?", user_id).Error
	return err
}
func FindAccountById(accountId int) (*models.AccountDto, error) {
	var accDto models.AccountDto
	err := config.Db.Model(&domain.Account{}).First(&accDto, "id = ?", accountId).Error
	return &accDto, err
}
