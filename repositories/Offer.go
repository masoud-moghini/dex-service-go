package repositories

import (
	"encoding/binary"
	"fmt"
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"

	"github.com/stellar/go/xdr"
)

func SaveOffer(offDto *models.OfferDto, legderId xdr.Int64) (models.OfferDto, error) {
	_LedgerId, err := legderId.MarshalBinary()
	LedgerId := binary.BigEndian.Uint64(_LedgerId)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(LedgerId)
	offer := &domain.Offer{
		AccountID:      offDto.AccountID,
		Amount:         offDto.Amount,
		LedgerID:       LedgerId,
		BuyingAssetID:  offDto.Buying,
		SellingAssetID: offDto.Selling,
		Price:          offDto.Price,
		Type:           offDto.Type,
	}
	offDto.OfferID = offer.ID
	err = config.Db.Model(&domain.Offer{}).Save(offer).Error
	return *offDto, err
}
