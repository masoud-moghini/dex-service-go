package repositories

import (
	"kuknos/dex/config"
	"kuknos/dex/domain"
	"kuknos/dex/models"

	"gorm.io/gorm"
)

func SaveUser(u *models.UserDto) error {
	user := domain.User{
		FirstName:    u.FirstName,
		LastName:     u.LastName,
		Email:        u.Email,
		PhoneNumber:  u.PhoneNumber,
		Iban:         u.Iban,
		NationalCode: u.NationalCode,
	}
	err := config.Db.Model(&domain.User{}).Create(&user).Error
	return err
}
func FindUser(id int) (*domain.User, *models.UserDto, error) {
	var oldUser domain.User
	err := config.Db.First(&oldUser, id).Error
	if err != nil {
		return nil, nil, err
	}
	var user = &models.UserDto{
		FirstName:    oldUser.FirstName,
		LastName:     oldUser.LastName,
		Email:        oldUser.Email,
		PhoneNumber:  oldUser.PhoneNumber,
		NationalCode: oldUser.NationalCode,
		Iban:         oldUser.Iban,
	}
	return &oldUser, user, nil
}
func UpdateUser(oldUser *domain.User, newUser *models.UserDto) *gorm.DB {
	oldUser.FirstName = newUser.FirstName
	oldUser.LastName = newUser.LastName
	oldUser.Iban = newUser.Iban
	oldUser.NationalCode = newUser.NationalCode
	return config.Db.Save(oldUser)
}
