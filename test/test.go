package main

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/stellar/go/xdr"
)

func main() {
	var opr xdr.OperationResult
	result_xdr := `AAAAAAAAw1AAAAAAAAAAAQAAAAAAAAADAAAAAAAAAAAAAAAAAAAAACP7/eyHNadGFYu5SULf1k10vLE6RPih3dZ/2bhNlC8aAAAAAAAAAGoAAAAAAAAAAVNLWUMAAAAAw7IoANxuejj7sSDv8gCBCBB0YXtQma36iqyaKO3F2EwAAAAAAJiWgAAAAAEAAAABAAAAAAAAAAAAAAAA`
	rawr := strings.NewReader(result_xdr)
	b64r := base64.NewDecoder(base64.StdEncoding, rawr)
	var tx xdr.TransactionResult
	_, err := xdr.Unmarshal(b64r, &tx)
	if err != nil {
		fmt.Println(err)
	}
	opr = (*tx.Result.Results)[0]
	fo := opr.Tr.ManageSellOfferResult
	res, succ := fo.GetSuccess()
	if !succ {
		fmt.Println(succ)
	}
	of, succ := res.Offer.GetOffer()
	if !succ {
		fmt.Println(succ)
	}
	fmt.Println("hi")
	fmt.Println(of.OfferId)
}
