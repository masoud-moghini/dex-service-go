module kuknos/dex

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/cosmtrek/air v1.21.2 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/mux v1.8.0
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/rs/cors v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stellar/go v0.0.0-20201218182351-013bf861cb0f
	github.com/stellar/go-xdr v0.0.0-20201028102745-f80a23dac78a
	golang.org/x/sys v0.0.0-20201204225414-ed752295db88 // indirect
	gorm.io/driver/postgres v1.0.5
	gorm.io/gorm v1.20.8
)
