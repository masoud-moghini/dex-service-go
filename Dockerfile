FROM golang:1.15
#FROM node:10.17.0-jessie
WORKDIR /app

COPY . .
#COPY ./index.js .


RUN go mod download
RUN go mod vendor
RUN go mod verify

RUN go build -o dex-services




#CMD [ "node","index.js" ]