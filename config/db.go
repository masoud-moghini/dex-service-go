package config

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type ConfigStruct struct {
	DbUsername  string
	DbPassword  string
	Dbname      string
	DbPort      string
	BaseAddress string
	Horizon     string
}

var Config ConfigStruct
var Db *gorm.DB
var err error

func init() {
	viper.AddConfigPath("config")
	viper.SetConfigType("yaml")
	if os.Getenv("PRODUCTION") != "" {

		viper.SetConfigName("remote")
		if err = viper.ReadInConfig(); err != nil {
			log.Fatal(err.Error())
		}
	} else {
		viper.SetConfigName("config")
		if err = viper.ReadInConfig(); err != nil {
			fmt.Println(err)
			if _, ok := err.(viper.ConfigFileNotFoundError); ok {
				panic("config file not found")
			} else {
				panic(err.Error())
			}
		}
	}
	host := (viper.Get("db.host")).(string)
	username := (viper.Get("db.username")).(string)
	password := (viper.Get("db.password")).(string)
	dbname := (viper.Get("db.dbname")).(string)
	port := (viper.Get("db.port")).(string)
	horizon := (viper.Get("backends.horizon")).(string)
	baseAddress := (viper.Get("backends.wallet")).(string)
	Config = ConfigStruct{
		DbUsername:  username,
		DbPassword:  password,
		Dbname:      dbname,
		DbPort:      port,
		BaseAddress: baseAddress,
		Horizon:     horizon,
	}
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Tehran", host, username, password, dbname, port)
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

}
