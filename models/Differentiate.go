package models

type Differentiate struct {
	BaseAsset         string  `json:"base_code"`
	CounterAsset      string  `json:"counter_code"`
	Change            float64 `json:"choice_price_change"`
	LastRecordedPrice int     `json:"last_recorded_price"`
}
