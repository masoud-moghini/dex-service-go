package models

type UserDto struct {
	FirstName    string `json:"first_name" valid:"required"`
	LastName     string `json:"last_name" valid:"required"`
	Email        string `json:"email" valid:"email"`
	PhoneNumber  string `json:"phone_number" valid:"required,matches(9\d{9})"`
	NationalCode string `json:"national_code" valid:""required,matches(\d[10])""`
	Iban         string `json:"iban" valid:"matches(IR\d{24})"`
}
