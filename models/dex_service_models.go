package models

import (
	"time"
)

type CronSummaryDto struct {
	StartedAt         time.Time "started_at"
	FinishedAt        time.Time "finished_at"
	Status            bool      "status"
	EstimatedDuration float64   "estimated_duration"
	ProcessedDuration float64   "processed_duration"
	RowsInserted      int16     "rows_inserted"
	RowsUpdated       int16     "rows_updated"
	RowsDeleted       int16     "rows_deleted"
	TotalRowsAffected int16     "total_rows_affected"
	Maintainer        string    "maintainer"
}

type MutualPriceDto struct {
	BaseAssetID    int     `json:"base_asset_id"`
	BaseAsset      string  `json:"base_asset_code"`
	BaseVolume     float64 `json:"base_volume"`
	CounterAsset   string  `json:"counter_asset_code"`
	CounterAssetID int     `json:"counter_asset_id"`
	Avg            float64
	Low            float64
	High           float64
	Open           float64
	Close          float64 `json:"close"`
}
