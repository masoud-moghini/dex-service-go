package models

type OfferDto struct {
	OfferID   uint    `json:"offer_id"`
	AccountID int     `json:"account_id`
	Selling   int     `json:"selling_asset_id"`
	Buying    int     `json:"buying_asset_id"`
	Type      string  `json:"type"` //buy or sell
	Amount    float64 `json:"amount"`
	Price     float64 `json:"price"`
	TX        string  `json:"tx"`
}
