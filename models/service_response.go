package models

import (
	"encoding/json"
)

type Links struct {
	self     string `json:"self"`
	next     string `json:"next"`
	previous string `json:"previous"`
}

type Meta struct {
	Success       bool        `json:"success"`
	DataType      interface{} `json:"data_type"`
	HasPagination bool        `json:"has_pagination"`
	Links         Links       `json:"links"`
}

type AssetSimplified struct {
	Code   string `json:"Code"`
	Issuer string `json:"issuer"`
}

type PriceRatio struct {
	Nominator   string `json:"n"`
	Denominator string `json:"d"`
}

type Message struct {
	Fa string `json:"fa"`
	En string `json:"en"`
}

type WalletServiceSuccessResponse struct {
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data"`
}

type WalletServiceFailureResponse struct {
	Meta    Meta    `json:"meta"`
	Message Message `json:"message"`
}

func ModelizeResponse(success bool, _data []byte, err error) (WalletServiceSuccessResponse, WalletServiceFailureResponse) {
	var meta Meta
	var data []map[string]interface{}
	var message Message

	if success {
		err1 := json.Unmarshal(_data, &data)
		if err1 != nil {
			panic(err1.Error())
		}
		meta = Meta{
			Success:       true,
			DataType:      "object",
			HasPagination: false,
			Links:         Links{},
		}
		data = data
		return WalletServiceSuccessResponse{
			Meta: meta,
			Data: data,
		}, WalletServiceFailureResponse{}

	}

	meta = Meta{
		Success:       false,
		DataType:      "object",
		HasPagination: false,
		Links:         Links{},
	}
	message = Message{
		Fa: "",
		En: err.Error(),
	}
	return WalletServiceSuccessResponse{}, WalletServiceFailureResponse{
		Meta:    meta,
		Message: message,
	}

}
func ModelizeSingleResponse(success bool, _data []byte, err error) (WalletServiceSuccessResponse, WalletServiceFailureResponse) {
	var meta Meta
	var data map[string]interface{}
	var message Message
	err1 := json.Unmarshal(_data, &data)
	if err1 != nil {
		panic(err1.Error())
	}
	if success {
		meta = Meta{
			Success:       true,
			DataType:      "object",
			HasPagination: false,
			Links:         Links{},
		}
		data = data
		return WalletServiceSuccessResponse{
			Meta: meta,
			Data: data,
		}, WalletServiceFailureResponse{}

	}
	meta = Meta{
		Success:       false,
		DataType:      "object",
		HasPagination: false,
		Links:         Links{},
	}
	message = Message{
		Fa: "",
		En: err.Error(),
	}
	return WalletServiceSuccessResponse{}, WalletServiceFailureResponse{
		Meta:    meta,
		Message: message,
	}

}
