package models

type Flags struct {
	auth_required  bool
	auth_revocable bool
	auth_immutable bool
}

type RelatedLinks struct {
	Self struct {
		Href string `json:"href"`
	} `json:"self"`
	Next struct {
		Href string `json:"href"`
	} `json:"next"`
	Prev struct {
		Href string `json:"href"`
	} `json:"prev"`
	Account     map[string]string `json:"account"`
	Ledger      map[string]string `json:"ledger"`
	Operations  map[string]string `json:"operations"`
	Effects     map[string]string `json:"effects"`
	Precedes    map[string]string `json:"precedes"`
	Succeeds    map[string]string `json:"succeeds"`
	Transaction map[string]string `json:"transaction"`
}

type TradeAggregationRecords struct {
	TradeCount    string `json:"trade_count"`
	Timestamp     int64  `json:"timestamp"`
	BaseVolume    string `json:"base_volume"`
	CounterVolume string `json:"counter_volume"`
	AveragePrice  string `json:"avg"`
	HighPrice     string `json:"high"`
	LowPrice      string `json:"low"`
	OpenPrice     string `json:"open"`
	ClosePrice    string `json:"close"`
	HighPriceR    struct {
		N map[string]int
		D map[string]int
	} `json:"high_r"`
	LowPriceR struct {
		N map[string]int
		D map[string]int
	} `json:"low_r"`
	OpenPriceR struct {
		N map[string]int
		D map[string]int
	} `json:"open_r"`
	ClosePriceR struct {
		N map[string]int
		D map[string]int
	} `json:"close_r"`
}

type EmbeddedRecords struct {
	Records []TradeAggregationRecords `json:"records"`
}

type HorizonTradeAggResponse struct {
	Links    RelatedLinks    `json:"_links"`
	Embedded EmbeddedRecords `json:"_embedded"`
}
type TransactionSubmition struct {
	Memo                  string `json:"memo"`
	Links                 Links  `json:"_links"`
	ID                    string `json:"id"`
	PagingToken           string `json:"paging_token"`
	Successful            bool   `json:"successful"`
	Hash                  string `json:"hash"`
	Ledger                int64  `json:"ledger"`
	SourceAccount         string `json:"source_account"`
	SourceAccountSequence string `json:"source_account_sequence"`
	FeeAccount            string `json:"fee_account"`
	OperationCount        int64  `json:"operation_count"`
	EnvelopeXDR           string `json:"envelope_xdr"`
	ResultXDR             string `json:"result_xdr"`
	FeeMetaXDR            string `json:"fee_meta_xdr"`
	MemoType              string `json:"memo_type"`
	Signatures            string `json:"signatures"`
}
