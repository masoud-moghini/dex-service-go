package models

type AccountDto struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Public string `json:"public_key"`
	UserId int    `json:"user_id"`
}
