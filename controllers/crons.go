package controllers

import (
	"encoding/json"
	"kuknos/dex/crons"
	"net/http"

	"github.com/gorilla/mux"
)

func cronsRoutes(r *mux.Router) {

	r.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]interface{}{"h": "sd"})

	})

	r.HandleFunc("/mutual-price", crons.MutualPriceCron)
	r.HandleFunc("/differentiate-price", crons.Differenctiate)
}
