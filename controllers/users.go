package controllers

import (
	"kuknos/dex/services"

	"github.com/gorilla/mux"
)

func usersRoutes(r *mux.Router) {
	r.HandleFunc("/create", services.CreateUser)
	r.HandleFunc("/edit/{user_id}", services.UserInfo).Methods("GET", "PUT")
}
