package controllers

import (
	"encoding/json"
	"kuknos/dex/services"
	"net/http"

	"github.com/gorilla/mux"
)

func marketsRoutes(r *mux.Router) {

	r.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(map[string]interface{}{"h": "sd"})

	})
	r.HandleFunc("/prices", services.LastPrices).Methods("GET")
	r.HandleFunc("/differentiates", services.Differenctiates).Methods("GET")
	r.HandleFunc("/assets", services.Assets)
}
