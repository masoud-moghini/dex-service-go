package controllers

import (
	"kuknos/dex/services"

	"github.com/gorilla/mux"
)

func accountsRoute(r *mux.Router) {
	r.HandleFunc("/create", services.CreateAccount)
	r.HandleFunc("/{user_id}/list", services.List)
	r.HandleFunc("/assets", services.UserDetail)
}
