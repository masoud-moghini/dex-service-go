package controllers

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func Serve() {
	r := mux.NewRouter()
	// r.HandleFunc("/test", func(h1 http.ResponseWriter, h2 *http.Request) {
	// 	h1.WriteHeader(http.StatusBadRequest)
	// 	json.NewEncoder(h1).Encode(map[string]interface{}{"one": "two"})
	// })

	c := cors.New(cors.Options{
		Debug:          true,
		AllowedHeaders: []string{"Token", "Content-Type", "public_id"},
	})
	handler := c.Handler(r)
	cronsRoutes(r.PathPrefix("/crons").Subrouter())
	marketsRoutes(r.PathPrefix("/markets").Subrouter())
	usersRoutes(r.PathPrefix("/users").Subrouter())
	accountsRoute(r.PathPrefix("/accounts").Subrouter())
	offersRoute(r.PathPrefix("/offers").Subrouter())
	http.ListenAndServe(":8080", handler)
}
