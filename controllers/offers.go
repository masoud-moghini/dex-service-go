package controllers

import (
	"kuknos/dex/services"

	"github.com/gorilla/mux"
)

func offersRoute(r *mux.Router) {
	r.HandleFunc("/create", services.CreateOffer).Methods("POST")
	r.HandleFunc("/list", services.ListOffers)
}
